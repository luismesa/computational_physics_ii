import math
from decimal import Decimal

def main():
    a = 0
    b = 5
    N = [1, 2, 4, 6, 8, 16, 32, 64, 128, 256]

    R_C = calculate_Romberg_table(a, b, N, evaluate_fresnel_arg_C)
    R_S = calculate_Romberg_table(a, b, N, evaluate_fresnel_arg_S)
    
    print("Romberg table for C(w)")
    print_Romberg_table(R_C)
    print("\nRomberg table for S(w)")
    print_Romberg_table(R_S)

def calculate_Romberg_table(a, b, N, integrand_function):
    R = []
    previous_row = None
    for n in N:
        r_n_1 = apply_trapezoidal_rule(a, b, n, integrand_function)
        row = calculate_full_Romberg_row(r_n_1, previous_row)
        R.append(row)
        previous_row = row

    return R

def calculate_full_Romberg_row(first_row_element, previous_row):
    current_row = []
    current_row.append(first_row_element)
    
    if(previous_row == None):
        return current_row
    
    current_row_lenght = len(previous_row) + 1
    for j in range(1, current_row_lenght):
        r_k_j_minus_1 = current_row[j-1]
        r_k_minus_1_j_minus_1 = previous_row[j-1]
        r_k_j = calculate_element_k_j(r_k_j_minus_1, r_k_minus_1_j_minus_1, j+1)
        current_row.append(r_k_j)
    
    return current_row

def calculate_element_k_j(r_k_j_minus_1, r_k_minus_1_j_minus_1, j):
    return r_k_j_minus_1 + (r_k_j_minus_1 - r_k_minus_1_j_minus_1)/(4**(j-1) - 1)

def apply_trapezoidal_rule(a, b, n, integrand_function):
    X = calculate_subsegments(a, b, n)
    
    h = X[1]-X[0] # step
    summatory = integrand_function(X[0]) + integrand_function(X[n])

    if(n > 1):
        for i in range(1, n): ## goes from 1 to n-1
            summatory = summatory + to_decimal(2) * integrand_function(X[i])

    result = h/2 * summatory

    return result

def calculate_subsegments(a, b, n):
    X = []

    h = (b-a)/n

    for i in range(0, n+1):
        x_i = to_decimal(a + i*h)
        X.append(x_i)
    
    return X

def evaluate_fresnel_arg_C(x):
    return cos(pi()/to_decimal(2) * x**2)

def evaluate_fresnel_arg_S(x):
    return sin(pi()/to_decimal(2) * x**2)

def to_decimal(f):
    return Decimal(str(f))

def cos(x):
    return to_decimal(math.cos(x))

def sin(x):
    return to_decimal(math.sin(x))

def pi():
    return to_decimal(math.pi)

def print_Romberg_table(R):
    for row_index, row in enumerate(R):
        row_display = ""
        for elem_index, element in enumerate(row):
            trunc_elem = round(element, 5)
            #row_display = row_display + "  R_{}_{}: {}".format(row_index+1, elem_index+1, trunc_elem)
            row_display = row_display + "  {}".format(trunc_elem)
        
        print(row_display)

main()