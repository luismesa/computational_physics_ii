import math
from decimal import Decimal
import matplotlib.pyplot as plt

def main():

    X_gaussian_cuadrature_t = [to_decimal(x) for x in Legendre_64_roots_raw()]
    X_gaussian_cuadrature = [recover_x_variable(t) for t in X_gaussian_cuadrature_t]
    X_gaussian_cuadrature.sort()
    Y_63_1 = [to_decimal(1) for i in range(0,64)]

    X_trapezoidal_rule_64 = [to_decimal(x) for x in calculate_subsegments(0,5,64)]
    Y_64_minus_1 = [to_decimal(-1) for i in range(0,65)]

    X_256_equidistant = [to_decimal(x) for x in calculate_subsegments(0,5,256)]
    Y_fresnel_arg_C = [evaluate_fresnel_arg_C(x) for x in X_256_equidistant]
    Y_fresnel_arg_S = [evaluate_fresnel_arg_S(x) for x in X_256_equidistant]

    print_clean_table(X_gaussian_cuadrature, X_trapezoidal_rule_64)
    
    plt.plot(X_trapezoidal_rule_64, Y_64_minus_1, "r.")
    plt.plot(X_gaussian_cuadrature, Y_63_1, "b.")
    plt.plot(X_256_equidistant, Y_fresnel_arg_C, "c-")
    plt.plot(X_256_equidistant, Y_fresnel_arg_S, "g-")

    plt.show()

def evaluate_fresnel_arg_C(x):
    return cos(pi()/to_decimal(2) * x**2)

def evaluate_fresnel_arg_S(x):
    return sin(pi()/to_decimal(2) * x**2)

def cos(x):
    return to_decimal(math.cos(x))

def sin(x):
    return to_decimal(math.sin(x))

def pi():
    return to_decimal(math.pi)

def print_clean_table(X_gaussian_cuadrature, X_equidistant):
    print("Gauss --- Equidistant")
    print("XXXXXX --- {}".format(X_equidistant[0] ))
    for i, x in enumerate(X_gaussian_cuadrature):
        print("{} --- {}".format(x, X_equidistant[i+1]))


def print_with_i(X_gaussian_cuadrature, X_equidistant):
    
    print("i --- Gauss({}) --- Equidistant({})".format(len(X_gaussian_cuadrature), len(X_equidistant)))
    print("{} --- XXXXXX --- {}".format(0, X_equidistant[0] ))
    for i, x in enumerate(X_gaussian_cuadrature):
        print("{} --- {} --- {}".format(i+1, x, X_equidistant[i+1]))


def recover_x_variable(t):
    return to_decimal(5/2) * (t+1)

def calculate_subsegments(a, b, n):
    X = []

    h = (b-a)/n

    for i in range(0, n+1):
        x_i = to_decimal(a + i*h)
        X.append(x_i)
    
    return X

def to_decimal(f):
    return Decimal(str(f))

def Legendre_64_roots_raw():
    return [
    -0.0243502926634244 
    ,0.0243502926634244  
    ,-0.0729931217877990 
    ,0.0729931217877990  
    ,-0.1214628192961206 
    ,0.1214628192961206  
    ,-0.1696444204239928 
    ,0.1696444204239928  
    ,-0.2174236437400071 
    ,0.2174236437400071  
    ,-0.2646871622087674 
    ,0.2646871622087674  
    ,-0.3113228719902110 
    ,0.3113228719902110  
    ,-0.3572201583376681 
    ,0.3572201583376681  
    ,-0.4022701579639916 
    ,0.4022701579639916  
    ,-0.4463660172534641 
    ,0.4463660172534641  
    ,-0.4894031457070530 
    ,0.4894031457070530  
    ,-0.5312794640198946 
    ,0.5312794640198946  
    ,-0.5718956462026340 
    ,0.5718956462026340  
    ,-0.6111553551723933 
    ,0.6111553551723933  
    ,-0.6489654712546573 
    ,0.6489654712546573  
    ,-0.6852363130542333 
    ,0.6852363130542333  
    ,-0.7198818501716109 
    ,0.7198818501716109  
    ,-0.7528199072605319 
    ,0.7528199072605319  
    ,-0.7839723589433414 
    ,0.7839723589433414  
    ,-0.8132653151227975 
    ,0.8132653151227975  
    ,-0.8406292962525803 
    ,0.8406292962525803  
    ,-0.8659993981540928 
    ,0.8659993981540928  
    ,-0.8893154459951141 
    ,0.8893154459951141  
    ,-0.9105221370785028 
    ,0.9105221370785028  
    ,-0.9295691721319396 
    ,0.9295691721319396  
    ,-0.9464113748584028 
    ,0.9464113748584028  
    ,-0.9610087996520538 
    ,0.9610087996520538  
    ,-0.9733268277899110 
    ,0.9733268277899110  
    ,-0.9833362538846260 
    ,0.9833362538846260  
    ,-0.9910133714767443 
    ,0.9910133714767443  
    ,-0.9963401167719553 
    ,0.9963401167719553  
    ,-0.9993050417357722 
    ,0.9993050417357722]

main()