import math
from decimal import Decimal

def main():
    a = 0
    b = 5
    N = [1, 2, 4, 6, 8, 16, 32, 64, 128, 256]

    results_table = []

    for n in N:
        I_C = apply_trapezoidal_rule(a, b, n, evaluate_fresnel_arg_C)
        I_S = apply_trapezoidal_rule(a, b, n, evaluate_fresnel_arg_S)
        results_table.append((n, I_C, I_S))
    
    print_results(results_table)

def apply_trapezoidal_rule(a, b, n, integral_argument):
    X = calculate_subsegments(a, b, n)
    
    h = X[1]-X[0] # step
    summatory = integral_argument(X[0]) + integral_argument(X[n])

    if(n > 1):
        for i in range(1, n): ## goes from 1 to n-1
            summatory = summatory + to_decimal(2) * integral_argument(X[i])

    result = h/2 * summatory

    return result

def calculate_subsegments(a, b, n):
    X = []

    h = (b-a)/n

    for i in range(0, n+1):
        x_i = to_decimal(a + i*h)
        X.append(x_i)
    
    return X

def evaluate_fresnel_arg_C(x):
    return cos(pi()/to_decimal(2) * x**2)

def evaluate_fresnel_arg_S(x):
    return sin(pi()/to_decimal(2) * x**2)

def to_decimal(f):
    return Decimal(str(f))

def cos(x):
    return to_decimal(math.cos(x))

def sin(x):
    return to_decimal(math.sin(x))

def pi():
    return to_decimal(math.pi)

def print_results(results_table):
    print("n ----> C(5) ----> S(5)")
    for r in results_table:
        print("{} ----> {} ----> {}".format(r[0], r[1], r[2]))

main()