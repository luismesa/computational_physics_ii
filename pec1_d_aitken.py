### FIXED POINT METHOD ###
alpha = 10**-2
def main():
    maximum_iterations = 100
    tolerance = 10**-5
    p_0_list = [0, 0.001, 0.02, 0.05, 0.1, 0.2, 0.4, 0.5]

    for p_0 in p_0_list:
        apply_fixed_point_method(p_0, tolerance, maximum_iterations)

def apply_fixed_point_method(p_0, tolerance, maximum_iterations):

    result = []

    p_n_minus_2 = 0
    p_n_minus_1 = 0
    p_n = p_0

    p_n_minus_2_aitken = 0

    for i in range(1,maximum_iterations):
        try:

            p_n_plus_1 = evaluate_function_g(p_n)
            
            p_n_minus_2 = p_n_minus_1
            p_n_minus_1 = p_n
            p_n = p_n_plus_1

            if(i>2):
                p_n_minus_3_aitken = p_n_minus_2_aitken
                p_n_minus_2_aitken = calculate_aitken_p_n_minus_2(p_n_minus_2, p_n_minus_1, p_n)
                if(abs(p_n_minus_2_aitken - p_n_minus_3_aitken) < tolerance):
                    print_aitken_result(p_n_minus_2_aitken, i, p_0, tolerance)
                    return

        except OverflowError:
            print_error("OverflowError", i, p_0, tolerance)
            return
    
    print("Tolerance not reached. Best value found => {}".format(p_n_plus_1))

def calculate_aitken_p_n_minus_2(p_n_minus_2, p_n_minus_1, p_n):
    return p_n_minus_2 - ((p_n_minus_1-p_n_minus_2)**2/(p_n - 2*p_n_minus_1 + p_n_minus_2))

def evaluate_function_g(r): 
    return r + alpha * evaluate_function(r)

def evaluate_function(r):
    return 1 - 10*r - 2*r**2

def print_aitken_result(result, iteration, seed, tolerance):
    aitken_index = iteration - 2
    print("i = {}; aitken_index = {}; tol = {}; p_0 = {}  => {}".format(iteration, aitken_index, tolerance, seed, result))


def print_error(error, iterations, seed, tolerance):
    print("i = {}; tol = {}; p_0 = {}  => error: {}".format(iterations, tolerance, seed, error))

main()