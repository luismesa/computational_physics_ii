import math
from decimal import Decimal
import matplotlib.pyplot as plt

def to_decimal(f):
    return Decimal(str(f))

## the function to evaluate is a sum over these data

red_triangles = "r^"
blue_dashes = "b--"
black_line = "g"

alpha = to_decimal(0.4)
beta = to_decimal(-2.019175541356262639422909427)

def main():
    plot_experimental_data(red_triangles)
    plot_least_squares_curve(blue_dashes)
    plot_pade_approximant(black_line)
    plt.show()

def plot_experimental_data(style):
    T_raw = [0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
    D_t_raw = [1.095, -0.1569, -1.0157, -1.4740, -1.3616, -0.8342, -0.0135]
    
    T = [to_decimal(t) for t in T_raw]
    D = [to_decimal(d) for d in D_t_raw]

    plt.plot(T, D, style)
    return 

def plot_least_squares_curve(style):
    D = []
    T = []
    for i in range(30):
        t = to_decimal(i) * to_decimal(10**-1)
        T.append(t)
        D.append(evaluate_least_squares_D(t))

    plt.plot(T, D, style)
    return 

def plot_pade_approximant(style):
    P = []
    T = []
    for i in range(30):
        t = to_decimal(i) * to_decimal(10**-1)
        T.append(t)
        P.append(evaluate_pade_approximant(t))

    plt.plot(T, P, black_line)

def evaluate_least_squares_D(t):
    return to_decimal(math.exp(-alpha*t)) + beta*to_decimal(math.sin(t))

def evaluate_pade_approximant(t):
    t_0 = to_decimal(1.5)

    denominator = 1 + (t-t_0) * (-alpha**3 * exp(-alpha*t_0) - beta*cos(t_0)) / (alpha**2 * exp(-alpha*t_0)- beta*sin(t_0))

    numerator_0 = exp(-alpha*t_0)+beta*sin(t_0)

    numerator_1 = beta*cos(t_0) - alpha*exp(-alpha*t_0) - ( (exp(-alpha*t_0)+beta*sin(t_0))*(-alpha**3*exp(-alpha*t_0)-beta*cos(t_0)) )/(alpha**2 * exp(-alpha*t_0) - beta*sin(t_0))

    numerator_2 = alpha**2*exp(-alpha*t_0) - beta*sin(t_0) - ( (beta*cos(t_0)-alpha*exp(-alpha*t_0)) * (-alpha**3 *exp(-alpha*t_0)-beta*cos(t_0)) )/(alpha**2 *exp(-alpha*t_0) - beta*sin(t_0))

    numerator = numerator_0+numerator_1*(t-t_0)+numerator_2*(t-t_0)**2

    return numerator/denominator

def cos(x):
    return to_decimal(math.cos(x))

def exp(x):
    return to_decimal(math.exp(x))

def sin(x):
    return to_decimal(math.sin(x))

main()