### BISECTION METHOD ###
def main():
    maximum_iterations = 1000
    segments_list = [(0.001, 3), (3, 6)]

    for segment in segments_list:
        result, message = apply_bisection_method(segment, maximum_iterations)
        print(message)

def apply_bisection_method(segment, maximum_iterations):

    result = []
    a_0 = segment[0]
    b_0 = segment[1]

    if(a_0 >= b_0):
        return result, "[a={},b={}] => Intervalo no válido. Debe cumplirse b>a.".format(a_0, b_0)

    if(evaluate_function(a_0)*evaluate_function(b_0) > 0):
        return result, "[a={},b={}] => Intervalo no válido. La función tiene el mismo signo en a y en b.".format(a_0, b_0)

    a_n = a_0
    b_n = b_0

    for i in range(maximum_iterations):
        p_n = a_n + (b_n - a_n)/2
        result.append(p_n)
        if(evaluate_function(p_n) * evaluate_function(a_n) > 0):
            a_n = p_n
        else:
            b_n = p_n

    return result, "[a={},b={}] => {}".format(a_0, b_0, p_n)

def evaluate_function(r):
    return 2*r**2 - 10*r + 1

#main()

