# Cubic spline interpolation

from decimal import Decimal
def to_decimal(f):
    return Decimal(str(f))

X_raw = [0.78,
0.80,
0.82,
0.84,
0.86,
0.88,
0.90,
0.92]

A_raw = [2053.9413,
1844.6480,
1640.5230,
1441.3171,
1246.7989,
1056.7528,
870.9777,
689.2859]

X_all = [to_decimal(x) for x in X_raw]
A_all = [to_decimal(a) for a in A_raw]



def main():
    x = to_decimal(0.8705)

    X = X_all[2:8]
    A = A_all[2:8]
    result, abs_error, j = execute_cubic_spline_interpolation(X, A, x)
    print_results(x, result, abs_error, j)

    X = X_all[3:8]
    A = A_all[3:8]
    result, abs_error, j = execute_cubic_spline_interpolation(X, A, x)
    print_results(x, result, abs_error, j)

def execute_cubic_spline_interpolation(X, A, x):

    n = len(X) - 1

    j = calculate_j(x, X) 

    deriv_f_0 = deriv_f(X[0])
    deriv_f_n = deriv_f(X[n])
    coefficients = calculate_cubic_coefficients(n, X, A, deriv_f_0, deriv_f_n)
   
    result = evaluate_S_j(x, j, X, coefficients)

    abs_error = calculate_abs_error(X)

    return result, abs_error, j

def calculate_j(x, X): # the subsegment [x_j, x_j+1] where x lies
    for j in range(0,len(X)-1):
        if(X[j]<x and x < X[j+1]):
            return j
    
    raise ValueError("x is not contained in any of the segments")

def evaluate_S_j(x, j, X, coefficients):
    A = coefficients[0]
    B = coefficients[1]
    C = coefficients[2]
    D = coefficients[3]

    S_j = A[j] + B[j]*(x - X[j]) + C[j]*(x - X[j])**2 + D[j]*(x - X[j])**3

    return S_j

def calculate_cubic_coefficients(n, X, A, deriv_f_0, deriv_f_n):
    H = calculate_H(n, X) 
    Alpha = calculate_Alpha(n, A, H, deriv_f_0, deriv_f_n)
    
    L, Miu, Z = calculate_L_Miu_Z(n, X, H, Alpha)


    return calculate_final_coefficients(n, A, H, L, Miu, Z)

def calculate_final_coefficients(n, A, H, L, Miu, Z):

    C = [None] * (n+1) # initialize C with n+1 positions
    B = [None] * n # initialize B with n positions
    D = [None] * n # initialize D with n positions
    C[n] = Z[n]

    for j in reversed(range(0, n)):
        C[j] = Z[j] - Miu[j]*C[j+1]
        B[j] = (A[j+1]-A[j])/H[j] - H[j]*(C[j+1]+2*C[j])/3
        D[j] = (C[j+1]-C[j])/(3*H[j])

    return [A[0:n], B[0:n], C[0:n], D[0:n]]

def calculate_L_Miu_Z(n, X, H, Alpha):
    
    L = []
    Miu = []
    Z = []

    l_0 = 2*H[0]
    miu_0 = to_decimal(0.5)
    z_0 = Alpha[0]/l_0

    L.append(l_0)
    Miu.append(miu_0)
    Z.append(z_0)

    for i in range(1,n):
        l_i = 2*(X[i+1]-X[i-1]) - H[i-1]*Miu[i-1]
        miu_i = H[i]/l_i
        z_i = (Alpha[i]-H[i-1]*Z[i-1])/l_i

        L.append(l_i)
        Miu.append(miu_i)
        Z.append(z_i)

    l_n = H[n-1]*(2-Miu[n-1])
    z_n = (Alpha[n]-H[n-1]*Z[n-1])/l_n

    L.append(l_n)
    Z.append(z_n)

    return L, Miu, Z


def calculate_Alpha(n, A, H, deriv_f_0, deriv_f_n):
    Alpha = []

    alpha_0 = 3*(A[1]-A[0])/H[0] - 3*deriv_f_0
    alpha_n = 3*deriv_f_n - 3*(A[n]-A[n-1])/H[n-1]

    Alpha.append(alpha_0)

    for i in range(1, n):
        alpha_i = 3*(A[i+1]-A[i])/H[i] - 3*(A[i]-A[i-1])/H[i-1]
        Alpha.append(alpha_i)

    Alpha.append(alpha_n)

    return Alpha

def calculate_H(n, X):
    H = []
    for i in range(0,n):
        H.append(X[i+1]-X[i])
    return H

def calculate_abs_error(X):
    x_min_interval = X[0]
    M = calculate_M(x_min_interval)
    max_h_4 = calculate_max_h_4(X)
    return to_decimal(5)*M/to_decimal(384) * max_h_4

def calculate_max_h_4(X):
    h_4_list = []
    for j in range(0, len(X)-1):
        h_4 = (X[j+1]-X[j])**4
        h_4_list.append(h_4)
    
    return max(h_4_list)

def calculate_M(min_x_interval):
    return abs(deriv_4_f(min_x_interval))

def deriv_f(x):
    return -to_decimal(5730)/(x * to_decimal(0.6932))

def deriv_4_f(x):
    return (1/x**4) * to_decimal(6)*to_decimal(5730)/to_decimal(0.6932)

def print_results(x, result, abs_error, j):
    print("x={}  =>  j={}".format(x, j))
    print("the estimated value for x={} is => {}".format(x, result))
    print("with absolue error => {}".format(abs_error))



main()