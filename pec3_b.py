# Diferencias divididas de Newton

from decimal import Decimal
def to_decimal(f):
    return Decimal(str(f))

X_raw = [0.78,
0.80,
0.82,
0.84,
0.86,
0.88,
0.90,
0.92]

A_raw = [2053.9413,
1844.6480,
1640.5230,
1441.3171,
1246.7989,
1056.7528,
870.9777,
689.2859]

X_all = [to_decimal(x) for x in X_raw]
A_all = [to_decimal(a) for a in A_raw]

def main():
    x = to_decimal(0.8705)

    initial_i = 2
    f_x_P_3_i_2 = calculate_f_x_aproximation_P_3(x, X_all, A_all, initial_i)
    f_x_P_4_i_2 = calculate_f_x_aproximation_P_4(x, X_all, A_all, initial_i)
    abs_error = abs(f_x_P_4_i_2 - f_x_P_3_i_2)
    print("i=2,3,4,5; n=3 : result => {}".format(f_x_P_3_i_2))
    print("with absolut error (next term rule) => {}".format(abs_error))
    print("i=2,3,4,5,6; n=4 : result => {}".format(f_x_P_4_i_2))


    initial_i = 3
    f_x_P_3_i_3 = calculate_f_x_aproximation_P_3(x, X_all, A_all, initial_i)
    f_x_P_4_i_3 = calculate_f_x_aproximation_P_4(x, X_all, A_all, initial_i)
    abs_error = abs(f_x_P_3_i_3 - f_x_P_4_i_3)
    print("i=3,4,5,6; n=3 : result => {}".format(f_x_P_3_i_3))
    print("with absolut error (next term rule) => {}".format(abs_error))
    print("i=3,4,5,6,7; n=4 : result => {}".format(f_x_P_4_i_3))

def calculate_f_x_aproximation_P_3(x, X, A, initial_i):
    n = 3
    X = X_all[initial_i : initial_i+n+1] 
    A = A_all[initial_i : initial_i+n+1] 
    F = [A]

    for j in range(1, n+1):
        column_j = calculate_column_j(j, X, F, n)
        F.append(column_j)

    coefficients = extract_diagonal(F, n)

    result = evaluate_P_3(x, X, coefficients)

    return result

def calculate_f_x_aproximation_P_4(x, X, A, initial_i):
    n = 4
    X = X_all[initial_i : initial_i+n+1] 
    A = A_all[initial_i : initial_i+n+1] 
    F = [A]

    for j in range(1, n+1):
        column_j = calculate_column_j(j, X, F, n)
        F.append(column_j)

    coefficients = extract_diagonal(F, n)

    result = evaluate_P_4(x, X, coefficients)

    return result

def evaluate_P_4(x, X, coefficients):
    term_0 = coefficients[0]
    term_1 = coefficients[1]*(x-X[0])
    term_2 = coefficients[2]*(x-X[0])*(x-X[1])
    term_3 = coefficients[3]*(x-X[0])*(x-X[1])*(x-X[2])
    term_4 = coefficients[4]*(x-X[0])*(x-X[1])*(x-X[2])*(x-X[3])
    return term_0+term_1+term_2+term_3+term_4

def evaluate_P_3(x, X, coefficients):
    term_0 = coefficients[0]
    term_1 = coefficients[1]*(x-X[0])
    term_2 = coefficients[2]*(x-X[0])*(x-X[1])
    term_3 = coefficients[3]*(x-X[0])*(x-X[1])*(x-X[2])
    return term_0+term_1+term_2+term_3

def calculate_column_j(j, X, F, n):
    column_j = []
    for i in range(0, n+1):
        if(i<j):
            column_j.append(None)
        else:
            element = (F[j-1][i] - F[j-1][i-1])/(X[i]-X[i-j])
            column_j.append(element)
    return column_j

def extract_diagonal(F, n):
    result = []
    for i in range(0,n+1):
        result.append(F[i][i])
    return result

main()