### FIXED POINT METHOD ###
alpha = 10**-2
def main():
    maximum_iterations = 100
    tolerance = 10**-5
    p_0_list = [0, 0.001, 0.02, 0.05, 0.1, 0.2, 0.4, 0.5]

    for p_0 in p_0_list:
        result, message = apply_fixed_point_method(p_0, tolerance, maximum_iterations)
        print(message)

def apply_fixed_point_method(p_0, tolerance, maximum_iterations):
    p_n = p_0
    result = [p_0]
    for i in range(maximum_iterations):
        try:
            p_n_plus_1 = evaluate_function_g(p_n)
            result.append(p_n_plus_1)
            if(abs(p_n_plus_1 - p_n) < tolerance):
                message = build_result_message(p_n_plus_1, i, p_0, tolerance)
                return result, message

            p_n = p_n_plus_1

        except OverflowError:
            error = build_error_message("OverflowError", i, p_0, tolerance)
            return result, error
    
    return "i = {}; tol = {} (NOT REACHED); p_0 = {}  => {}".format(i, tolerance, p_0, p_n)

def evaluate_function_g(r): 
    return r + alpha * evaluate_function(r)

def evaluate_function(r):
    return 1 - 10*r - 2*r**2

def build_result_message(result, iterations, seed, tolerance):
    return "i = {}; tol = {}; p_0 = {}  => {}".format(iterations, tolerance, seed, result)

def build_error_message(error, iterations, seed, tolerance):
    return "i = {}; tol = {}; p_0 = {}  => error: {}".format(iterations, tolerance, seed, error)

main()