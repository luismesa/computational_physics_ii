##  CONVERGENCE COMPARISON
from pec1_a import apply_newton_method
from pec1_c import apply_bisection_method
from pec1_d import apply_fixed_point_method

def main():
    
    newton_series, newton_message = apply_newton_method(p_0=0.01, tolerance=10**-5, maximum_iterations=100)
    bisection_series, bisection_message = apply_bisection_method(segment=(0.001, 3), maximum_iterations=100)
    fixed_point_series, fixed_point_message = apply_fixed_point_method(p_0=0.02, tolerance=10**-5, maximum_iterations=100)

    print("Newton Method:")
    print_series(newton_series)
    print("Bisection Method:")
    print_series(bisection_series)
    print("Fixed Point Method:")
    print_series(fixed_point_series)

def print_series(series):
    for i, element in enumerate(series):
        print("i={} => {}".format(i, element))

main()