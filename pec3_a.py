# Order 2 Lagrange polynomial interpolation

from decimal import Decimal
def to_decimal(f):
    return Decimal(str(f))

x_3 = to_decimal(0.84)
f_3 = to_decimal(1441.3171)

x_4 = to_decimal(0.86)
f_4 = to_decimal(1246.7989)

x_5 = to_decimal(0.88)
f_5 = to_decimal(1056.7528)

x_6 = to_decimal(0.90)
f_6 = to_decimal(870.9777)

def main():
    x = to_decimal(0.8705)

    P_1_for_i_4_5 = evaluate_P_1(x, x_4, x_5, f_4, f_5)
    print("P_1 for i=4,5 => {}".format(P_1_for_i_4_5))

    P_2_for_i_3_4_5 = evaluate_P_2(x, x_3, x_4, x_5, f_3, f_4, f_5)
    P_2_for_i_3_4_5_abs_error = abs(P_2_for_i_3_4_5 - P_1_for_i_4_5)
    print("P_2 for i=3,4,5 => {}".format(P_2_for_i_3_4_5))
    print("with absolute error => {}".format(P_2_for_i_3_4_5_abs_error))

    P_2_for_i_4_5_6 = evaluate_P_2(x, x_4, x_5, x_6, f_4, f_5, f_6)
    P_2_for_i_4_5_6_abs_error = abs(P_2_for_i_4_5_6 - P_1_for_i_4_5)
    print("P_2 for i=4,5,6 => {}".format(P_2_for_i_4_5_6))
    print("with absolute error => {}".format(P_2_for_i_4_5_6_abs_error))

def evaluate_P_2(x, x_a, x_b, x_c, f_a, f_b, f_c):
    term_a = f_a*(((x-x_b)*(x-x_c))/((x_a-x_b)*(x_a-x_c)))
    term_b = f_b*(((x-x_a)*(x-x_c))/((x_b-x_a)*(x_b-x_c)))
    term_c = f_c*(((x-x_a)*(x-x_b))/((x_c-x_a)*(x_c-x_b)))
    return term_a + term_b + term_c

def evaluate_P_1(x, x_a, x_b, f_a, f_b):
    term_a = f_a*((x-x_b)/(x_a-x_b))
    term_b = f_b*((x-x_a)/(x_b-x_a))
    return term_a + term_b

main()