import math
from decimal import Decimal
import matplotlib.pyplot as plt

def to_decimal(f):
    return Decimal(str(f))

## the function to evaluate is a sum over these data
T_raw = [0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
D_t_raw = [1.095, -0.1569, -1.0157, -1.4740, -1.3616, -0.8342, -0.0135]

T = [to_decimal(t) for t in T_raw]
D = [to_decimal(d) for d in D_t_raw]

def plot_der_f():
    X = []
    Y = []
    Z = []
    for i in range(len(T)):
        x = i
        y = evaluate_derivative_function(x)
        d = evaluate_function(x)
        X.append(x)
        Y.append(y)
        Z.append(d)
    plt.plot(X, Y, "ro")
    plt.plot(X, Z, "g")
    plt.show()

def main():

    maximum_iterations = 100
    precision = to_decimal(10**-8)
    seed = to_decimal(0)

    alpha_with_errors, message = apply_newton_method(seed, precision, maximum_iterations)
    alpha = alpha_with_errors[-1][0]
    beta = calculate_beta(alpha)

    print("alpha => {}".format(alpha))
    print("beta => {}".format(beta))

def calculate_beta(alpha):
    return calculate_numerator_summatory(T,D,alpha)/calculate_denominator_summatory(T,D,alpha)


# returns the a list of tuples, where each tuple is the value for that iteration and its error,
# the error being the difference between the value and the previous
#  the last one is the best value and the error obtained when the method stopped
def apply_newton_method(seed, precision, maximum_iterations):

    alpha_n = seed
    result = [(alpha_n, None)]
    for i in range(maximum_iterations):
        try:
            alpha_n_plus_1 = calculate_alpha_n_plus_1(alpha_n, T, D)
            error_alpha_n_plus_1 = abs(alpha_n_plus_1 - alpha_n) 
            result.append((alpha_n_plus_1, error_alpha_n_plus_1))

            if(error_alpha_n_plus_1 < precision):
                return result, build_result_message(alpha_n, i, seed, precision)
            
            alpha_n = alpha_n_plus_1

        except ZeroDivisionError:
            return result, build_error_message("ZeroDivisionError has occurred", i, seed, precision)

    return result, build_not_reached_message(alpha_n, i, seed, precision)

def calculate_alpha_n_plus_1(alpha_n, T, D):

    return alpha_n - evaluate_function(alpha_n)/evaluate_derivative_function(alpha_n)

def evaluate_function(alpha):
    
    numerator_summatory = calculate_numerator_summatory(T, D, alpha)
    denominator_summatory = calculate_denominator_summatory(T, D, alpha)

    result = calculate_full_summatory(T, D, alpha, numerator_summatory, denominator_summatory)

    return result

def evaluate_derivative_function(alpha):
    
    numerator_summatory = calculate_numerator_summatory(T, D, alpha)
    numerator_derivative_sumatory = calculate_numerator_derivative_sumatory(T,D,alpha)

    denominator_summatory = calculate_denominator_summatory(T, D, alpha)
    denominator_derivative_summatory = calculate_denominator_derivative_summatory(T,D,alpha)

    result = calculate_full_derivative_summatory(T, D, alpha, numerator_summatory, denominator_summatory, numerator_derivative_sumatory, denominator_derivative_summatory)

    return result

def calculate_full_summatory(T, D, alpha, numerator_summatory, denominator_summatory):
    result = 0
    for i in range(len(T)):
        i_th_term = to_decimal(-2) * to_decimal(math.sin(T[i])) * (D[i] - to_decimal(math.exp(-alpha*T[i])) - to_decimal(math.sin(T[i]))*numerator_summatory/denominator_summatory)
        result = result + i_th_term

    return result


def calculate_numerator_summatory(T, D, alpha):
    result = 0
    for i in range(len(T)):
        i_th_term = to_decimal(2) * T[i] * to_decimal(math.exp(-alpha*T[i])) * (D[i] - to_decimal(math.exp(-alpha*T[i])))
        result = result + i_th_term
    
    return result

def calculate_denominator_summatory(T, D, alpha):
    result = 0
    for i in range(len(T)):
        i_th_term = to_decimal(2) * T[i] * to_decimal(math.sin(T[i])) * to_decimal(math.exp(-alpha*T[i]))
        result = result + i_th_term
    
    return result

def calculate_full_derivative_summatory(T, D, alpha, numerator_summatory, denominator_summatory, numerator_derivative_sumatory, denominator_derivative_summatory):
    result = 0
    for i in range(len(T)):
        quotient_summatory = ((numerator_derivative_sumatory*denominator_summatory) - (numerator_derivative_sumatory*denominator_summatory))/denominator_summatory**2
        i_th_term = to_decimal(-2) * to_decimal(math.sin(T[i])) * ( (T[i] * to_decimal(math.exp(-alpha*T[i]))) - (to_decimal(math.sin(T[i])) * quotient_summatory) )
        result = result + i_th_term

    return result

def calculate_denominator_derivative_summatory(T,D,alpha):
    result = 0
    for i in range(len(T)):
        i_th_term = to_decimal(-2) * T[i]**2 * to_decimal(math.exp(-alpha*T[i])) * to_decimal(math.sin(T[i]))
        result = result + i_th_term

    return result

def calculate_numerator_derivative_sumatory(T,D,alpha):
    result = 0
    for i in range(len(T)):
        i_th_term = to_decimal(-2) * T[i]**2 * to_decimal(math.exp(to_decimal(-2)*alpha*T[i])) * (D[i]*to_decimal(math.exp(alpha*T[i])) - to_decimal(2))
        result = result + i_th_term

    return result


def build_result_message(result, iterations, seed, precision):
    return "i = {}; tol = {}; alpha_0 = {}  => {}".format(iterations, precision, seed, result)

def build_not_reached_message(result, iterations, seed, precision):
    return "i = {}; tol = {} (NOT REACHED); alpha_0 = {}  => {}".format(iterations, precision, seed, result)

def build_error_message(error, iterations, seed, precision):
    return "i = {}; tol = {}; alpha_0 = {}  => error: {}".format(iterations, precision, seed, error)

def pretty_print_tuple_list(tuple_list):
    print("alpha --------------------> error")
    for i in range(len(tuple_list)):
        current = tuple_list[i]
        print("{} -> {}".format(current[0], current[1]))
main()