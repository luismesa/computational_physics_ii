import math
from decimal import Decimal
import matplotlib.pyplot as plt

def to_decimal(f):
    return Decimal(str(f))

## the function to evaluate is a sum over these data
T_raw = [0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
D_t_raw = [1.095, -0.1569, -1.0157, -1.4740, -1.3616, -0.8342, -0.0135]

T = [to_decimal(t) for t in T_raw]
D = [to_decimal(d) for d in D_t_raw]


alpha = to_decimal(0.4)
beta = to_decimal(-2.019175541356262639422909427)

def plot_abs_error_around_alpha(style):
    X = []
    Y = []
    for i in range(20):
        alpha_i = to_decimal(0.01*i) + to_decimal(0.3)
        abs_error = evaluate_abs_error(alpha_i, beta)
        X.append(alpha_i)
        Y.append(abs_error)
    plt.plot(X, Y, style)

def plot_abs_error_around_beta(style):
    X = []
    Y = []
    for i in range(20):
        beta_i = to_decimal(0.01*i) + to_decimal(-2.1)
        abs_error = evaluate_abs_error(alpha, beta_i)
        X.append(beta_i)
        Y.append(abs_error)
    plt.plot(X, Y, style)

def main():       

    plot_abs_error_around_alpha("b--")
    plot_abs_error_around_beta("r--")

    #plt.savefig("plot.png")
    plt.show()

def evaluate_abs_error(alpha, beta):
    result = 0
    for i in range(len(T)):
        i_th = (D[i] - evaluate_theoretical_D(alpha, beta, T[i]))**2
        result = result + i_th

    return result

def evaluate_theoretical_D(alpha, beta, t):
    return exp(-alpha*t) + beta*sin(t)

def cos(x):
    return to_decimal(math.cos(x))

def exp(x):
    return to_decimal(math.exp(x))

def sin(x):
    return to_decimal(math.sin(x))


main()