### NEWTON METHOD ###

#Conclusiones
#Si la semilla es mayor que el máximo de la función, la asíntota engaña al método "halando" hacia el cero en el infinito que no llega.
#Solo funcionan las semillas entre 0 y el pico de la función, que es donde el cero real "hala" al método de Newton.
#No analizo los casos negativos porque físicamente no tienen sentido.

# calculemos los ceros de la derivada del Vef: dVef/dr = 0
# el cero se calcula de manera analítica directamente, y es en r = 1/5 = 0.2
# r>0

def main():
    maximum_iterations = 100
    tolerance = 10**-5
    p_0_list = [0.01, 0.1, 0.2, 0.25, 0.29, 0.3, 0.30001, 0.31, 0.4, 0.5, 0.6, 1, 2, 3, 4, 5, 10, 20, 30, 40, 100, 200]

    for p_0 in p_0_list:
        result, message = apply_newton_method(p_0, tolerance, maximum_iterations)
        print(message)

def apply_newton_method(p_0, tolerance, maximum_iterations):

    p_n = p_0
    result = [(p_0, None)]
    for i in range(maximum_iterations):
        try:
            p_n_plus_1 = calculate_p_n_plus_1(p_n)
            error_p_n_plus_1 = abs(p_n_plus_1 - p_n)
            result.append((p_n_plus_1, error_p_n_plus_1))

            if (error_p_n_plus_1 < tolerance):
                return result, build_result_message(p_n_plus_1, error_p_n_plus_1, i, p_0, tolerance)
            
            p_n = p_n_plus_1

        except ZeroDivisionError:
            return result, build_error_message("Derivative is 0", i, p_0, tolerance)

    return result, build_not_reached_message(p_n, i, p_0, tolerance)

def calculate_p_n_plus_1(p_n):
    return p_n - evaluate_function(p_n)/evaluate_derivative_function(p_n)

def evaluate_function(r):
    return (5*r-1)/r**3

def evaluate_derivative_function(r):
    return -(10*r-3)/r**4

def build_result_message(result, error, iterations, seed, tolerance):
    return "i = {}; tol = {}; p_0 = {}:\n {}  error: {}".format(iterations, tolerance, seed, result, error)

def build_not_reached_message(result, iterations, seed, tolerance):
    return "i = {}; tol = {} (NOT REACHED); p_0 = {}  => {}".format(iterations, tolerance, seed, result)

def build_error_message(error, iterations, seed, tolerance):
    return "i = {}; tol = {}; p_0 = {}  => error: {}".format(iterations, tolerance, seed, error)

main()